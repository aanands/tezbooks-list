class TransactionsDataModel {
  String name;
  String status;
  String stage;
  String date;
  TransactionsDataModel({
    required this.name,
    required this.status,
    required this.stage,
    required this.date,
  });
}

class TransactionModel {
  List<TransactionsDataModel>? transactionData;
  String? errorMsg;

  TransactionModel.fromJson(Map<String, dynamic> json) {
    transactionData = [];
    for (int i = 0; i < json["data"].length; i++) {
      TransactionsDataModel temp = TransactionsDataModel(
        name: json["data"][i]["name"],
        date: json["data"][i]["date"],
        status: json["data"][i]["status"],
        stage: json["data"][i]["stage"],
      );
      transactionData!.add(temp);
    }
  }
  TransactionModel.withError(String error) : errorMsg = error;
}
