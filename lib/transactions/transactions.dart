import 'package:flutter/material.dart';
import 'package:tezbooks/transactions/bloc/transaction_bloc.dart';
import 'package:tezbooks/transactions/bloc/transaction_event.dart';
import 'package:tezbooks/transactions/bloc/transaction_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TransactionScreen extends StatefulWidget {
  const TransactionScreen({super.key});

  @override
  State<TransactionScreen> createState() => _TransactionScreenState();
}

class _TransactionScreenState extends State<TransactionScreen> {
  final TransactionBloc transactionBloc = TransactionBloc();

  @override
  void initState() {
    super.initState();
    transactionBloc.add(FetchTransactionData());
  }

  @override
  Widget build(BuildContext context) {
    List values = [
      'Paid',
      'Unpaid',
      'Reimbrushable',
      'Edit',
      'Review',
      'Approve',
    ];
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      body: BlocBuilder<TransactionBloc, TransactionState>(
        bloc: transactionBloc,
        builder: (context, state) {
          if (state is TransactionLoaded) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      if (state.bulkActionSelect.isNotEmpty)
                        GestureDetector(
                          onTap: () {
                            transactionBloc
                                .add(ResetSelectionTransactionData());
                          },
                          child: const Align(
                            alignment: Alignment.centerRight,
                            child: Icon(Icons.close),
                          ),
                        ),
                      if (state.bulkActionSelect.isNotEmpty)
                        Align(
                          alignment: Alignment.centerLeft,
                          child:
                              Text("${state.bulkActionSelect.length} Selected"),
                        ),
                      if (state.bulkActionSelect.isNotEmpty)
                        const Align(
                          alignment: Alignment.centerRight,
                          child: Icon(Icons.delete),
                        ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: GestureDetector(
                          onTap: () {
                            showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return BlocBuilder<TransactionBloc,
                                    TransactionState>(
                                  bloc: transactionBloc,
                                  builder: (context, state) {
                                    if (state is TransactionLoaded) {
                                      return AlertDialog(
                                        title:
                                            const Text('Filter Transactions'),
                                        content: SizedBox(
                                          width: double.maxFinite,
                                          child: SingleChildScrollView(
                                            child: Column(
                                              mainAxisSize: MainAxisSize.min,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                ListView.builder(
                                                  itemCount: values.length,
                                                  shrinkWrap: true,
                                                  scrollDirection:
                                                      Axis.vertical,
                                                  itemBuilder:
                                                      (context, index) {
                                                    return ListTile(
                                                      leading: Checkbox(
                                                        value: state
                                                            .filterSelect
                                                            .contains(index),
                                                        onChanged:
                                                            (bool? value) {
                                                          transactionBloc.add(
                                                            SetTransactionFilter(
                                                                index, value!),
                                                          );
                                                          // // print(state.filterSelect
                                                          //     .contains(index));
                                                        },
                                                      ),
                                                      title:
                                                          Text(values[index]),
                                                    );
                                                  },
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                        actions: <Widget>[
                                          TextButton(
                                            onPressed: () {
                                              transactionBloc.add(
                                                  ResetTransactionFilter());
                                              transactionBloc
                                                  .add(FetchTransactionData());
                                              Navigator.of(context).pop();
                                            },
                                            child: const Text('Reset Filters'),
                                          ),
                                          TextButton(
                                            onPressed: () {
                                              if (state
                                                  .filterSelect.isNotEmpty) {
                                                String status =
                                                    (state.filterSelect[0])
                                                        .toString();
                                                String stage = state
                                                            .filterSelect[1] >=
                                                        3
                                                    ? (state.filterSelect[1] -
                                                            3)
                                                        .toString()
                                                    : (state.filterSelect[1])
                                                        .toString();
                                                transactionBloc.add(
                                                  FetchFilteredTransactionData(
                                                      status, stage),
                                                );
                                              }
                                              Navigator.of(context).pop();
                                            },
                                            child: const Text('Apply Filters'),
                                          ),
                                        ],
                                      );
                                    }
                                    return const Center(
                                      child: CircularProgressIndicator(),
                                    );
                                  },
                                );
                              },
                            );
                          },
                          child: const Icon(Icons.filter_list_alt),
                        ),
                      ),
                    ],
                  ),
                  SingleChildScrollView(
                    child: ListView.builder(
                      itemCount: state.transactionModel.transactionData!.length,
                      shrinkWrap: true,
                      scrollDirection: Axis.vertical,
                      itemBuilder: (context, index) {
                        if (state.bulkActionSelect.isNotEmpty) {
                          if (state.bulkActionSelect.contains(index)) {
                            return ListTile(
                              leading: const Icon(Icons.done_outline_outlined),
                              title: Text(state.transactionModel
                                  .transactionData![index].name),
                              subtitle: Row(
                                children: [
                                  Text(state.transactionModel
                                      .transactionData![index].status),
                                  const SizedBox(
                                    width: 20,
                                  ),
                                  Text(state.transactionModel
                                      .transactionData![index].stage),
                                ],
                              ),
                              trailing: Text(state.transactionModel
                                  .transactionData![index].date),
                              onTap: () {
                                transactionBloc
                                    .add(SelectTransactionData(index, false));
                              },
                            );
                          }
                          return ListTile(
                            leading: const Icon(Icons.monetization_on_sharp),
                            title: Text(state
                                .transactionModel.transactionData![index].name),
                            subtitle: Row(
                              children: [
                                Text(state.transactionModel
                                    .transactionData![index].status),
                                const SizedBox(
                                  width: 20,
                                ),
                                Text(state.transactionModel
                                    .transactionData![index].stage),
                              ],
                            ),
                            trailing: Text(state
                                .transactionModel.transactionData![index].date),
                            onTap: () {
                              transactionBloc
                                  .add(SelectTransactionData(index, true));
                            },
                          );
                        }
                        return ListTile(
                          leading: const Icon(Icons.monetization_on_sharp),
                          title: Text(state
                              .transactionModel.transactionData![index].name),
                          subtitle: Row(
                            children: [
                              Text(state.transactionModel
                                  .transactionData![index].status),
                              const SizedBox(
                                width: 20,
                              ),
                              Text(state.transactionModel
                                  .transactionData![index].stage),
                            ],
                          ),
                          trailing: Text(state
                              .transactionModel.transactionData![index].date),
                          onLongPress: () {
                            transactionBloc
                                .add(SelectTransactionData(index, true));
                          },
                        );
                      },
                    ),
                  ),
                ],
              ),
            );
          }
          if (state is TransactionError) {
            return Center(
              child: Text(state.error!),
            );
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
