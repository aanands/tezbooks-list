import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:tezbooks/transactions/model/transaction_model.dart';

class TransactionApiProvider {
  Future<TransactionModel> getTransactionData(
    String status,
    String stage,
  ) async {
    String baseUrl = "http://localhost:8000?status=$status&stage=$stage";
    dynamic res;
    try {
      await Future.delayed(const Duration(seconds: 2));
      res = await http.get(Uri.parse(baseUrl)).timeout(
            const Duration(seconds: 5),
          );
    } catch (e) {
      return TransactionModel.withError("Failed to fetch data");
    }
    if (res.statusCode == 200) {
      return TransactionModel.fromJson(json.decode(res.body));
    }
    return TransactionModel.withError("Failed to fetch data");
  }
}
