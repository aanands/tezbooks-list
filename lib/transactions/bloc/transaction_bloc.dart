import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tezbooks/transactions/bloc/transaction_event.dart';
import 'package:tezbooks/transactions/bloc/transaction_state.dart';
import 'package:tezbooks/transactions/api/transaction_api.dart';

class TransactionBloc extends Bloc<TransactionEvent, TransactionState> {
  final TransactionApiProvider transactionApiProvider =
      TransactionApiProvider();

  TransactionBloc() : super(TransactionInitial()) {
    on<FetchTransactionData>(
      (event, emit) async {
        emit(TransactionLoading());

        try {
          final res = await transactionApiProvider.getTransactionData('', '');

          emit(TransactionLoaded(res, [], []));
          if (res.errorMsg != null) {
            emit(TransactionError(res.errorMsg));
          }
        } catch (e) {
          emit(TransactionError("Failed to fetch data"));
        }
      },
    );
    on<SetTransactionFilter>(
      (event, emit) {
        if (state is TransactionLoaded) {
          if (event.state) {
            (state as TransactionLoaded).filterSelect.add(event.index);
          } else {
            (state as TransactionLoaded).filterSelect.remove(event.index);
          }
          emit(TransactionLoaded(
            (state as TransactionLoaded).transactionModel,
            (state as TransactionLoaded).filterSelect,
            (state as TransactionLoaded).bulkActionSelect,
          ));
        }
      },
    );

    on<ResetTransactionFilter>(
      (event, emit) {
        if (state is TransactionLoaded) {
          (state as TransactionLoaded).filterSelect.clear();
          emit(TransactionLoaded(
            (state as TransactionLoaded).transactionModel,
            (state as TransactionLoaded).filterSelect,
            (state as TransactionLoaded).bulkActionSelect,
          ));
        }
      },
    );
    on<FetchFilteredTransactionData>(
      (event, emit) async {
        List<int> temp = (state as TransactionLoaded).filterSelect;

        emit(TransactionLoading());

        try {
          final res = await transactionApiProvider.getTransactionData(
              event.status, event.stage);

          emit(TransactionLoaded(res, temp, []));
          if (res.errorMsg != null) {
            emit(TransactionError(res.errorMsg));
          }
        } catch (e) {
          emit(TransactionError("Failed to fetch data"));
        }
      },
    );
    on<SelectTransactionData>(
      (event, emit) {
        if (state is TransactionLoaded) {
          if (event.state) {
            (state as TransactionLoaded).bulkActionSelect.add(event.index);
          } else {
            (state as TransactionLoaded).bulkActionSelect.remove(event.index);
          }
          emit(TransactionLoaded(
            (state as TransactionLoaded).transactionModel,
            (state as TransactionLoaded).filterSelect,
            (state as TransactionLoaded).bulkActionSelect,
          ));
        }
      },
    );
    on<ResetSelectionTransactionData>(
      (event, emit) {
        if (state is TransactionLoaded) {
          (state as TransactionLoaded).bulkActionSelect.clear();
          emit(TransactionLoaded(
            (state as TransactionLoaded).transactionModel,
            (state as TransactionLoaded).filterSelect,
            (state as TransactionLoaded).bulkActionSelect,
          ));
        }
      },
    );
  }
}
