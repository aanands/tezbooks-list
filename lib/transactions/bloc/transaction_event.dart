abstract class TransactionEvent {}

class FetchTransactionData extends TransactionEvent {}

class SetTransactionFilter extends TransactionEvent {
  int index;
  bool state;
  SetTransactionFilter(this.index, this.state);
}

class ResetTransactionFilter extends TransactionEvent {}

class FetchFilteredTransactionData extends TransactionEvent {
  String status;
  String stage;
  FetchFilteredTransactionData(this.status, this.stage);
}

class SelectTransactionData extends TransactionEvent {
  int index;
  bool state;
  SelectTransactionData(this.index, this.state);
}

class ResetSelectionTransactionData extends TransactionEvent {}
