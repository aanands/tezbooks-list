import 'package:tezbooks/transactions/model/transaction_model.dart';

abstract class TransactionState {}

class TransactionInitial extends TransactionState {}

class TransactionLoading extends TransactionState {}

class TransactionLoaded extends TransactionState {
  final TransactionModel transactionModel;
  List<int> filterSelect;
  List<int> bulkActionSelect;
  TransactionLoaded(
      this.transactionModel, this.filterSelect, this.bulkActionSelect);
}

class TransactionError extends TransactionState {
  final String? error;
  TransactionError(this.error);
}
