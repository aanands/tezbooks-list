import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tezbooks/transactions/bloc/transaction_bloc.dart';

import 'package:tezbooks/transactions/transactions.dart';

void main(List<String> args) {
  runApp(const TezBooksApp());
}

class TezBooksApp extends StatefulWidget {
  const TezBooksApp({super.key});

  @override
  State<TezBooksApp> createState() => _TezBooksAppState();
}

class _TezBooksAppState extends State<TezBooksApp> {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => TransactionBloc(),
        ),
      ],
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: TransactionScreen(),
      ),
    );
  }
}
